#include <tagcompound.hpp>

namespace NBT
{

TagCompound::TagCompound()
{
}

TagCompound::~TagCompound()
{
}

void TagCompound::read(Flot& f)
{
	children.clear();

	std::shared_ptr<Tag> tag;
    while((tag = readTag(f))->type() != TAG_END)
		children[tag->name()] = tag;
}

void TagCompound::write(Flot& f) const
{
	for(std::map<std::string, std::shared_ptr<Tag>>::const_iterator it = children.begin(); it != children.end(); it++)
        writeTag(it->second, f);
	f << uint8_t(0);
}

void TagCompound::set(const std::string& name, const std::shared_ptr<Tag>& value)
{
	value->setName(name);
	children[name] = value;
}

void TagCompound::set(const std::shared_ptr<Tag>& value)
{
	set(value->name(), value);
}

const std::shared_ptr<Tag> TagCompound::get(const std::string& name)
{
	std::map<std::string, std::shared_ptr<Tag>>::iterator it = children.find(name);
    if (it == children.end()) {
		return nullptr;
	}
    
	return it->second;
}

}