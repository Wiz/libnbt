#ifndef __FLOT__
#define __FLOT__

#include <iostream>
#include <stdint.h>

namespace NBT
{

class Flot
{
protected:
public:
	Flot();
	virtual ~Flot();

	virtual int read(const uint8_t* buf, size_t len) = 0;
	virtual void write(const uint8_t* buf, size_t len) = 0;

	Flot& operator<<(int8_t value);
	Flot& operator<<(uint8_t value);
	Flot& operator<<(int16_t value);
	Flot& operator<<(uint16_t value);
	Flot& operator<<(int32_t value);
	Flot& operator<<(uint32_t value);
	Flot& operator<<(int64_t value);
	Flot& operator<<(uint64_t value);
	Flot& operator<<(float value);
	Flot& operator<<(double value);
	Flot& operator<<(const std::string& value);

	Flot& operator>>(int8_t& value);
	Flot& operator>>(uint8_t& value);
	Flot& operator>>(int16_t& value);
	Flot& operator>>(uint16_t& value);
	Flot& operator>>(int32_t& value);
	Flot& operator>>(uint32_t& value);
	Flot& operator>>(int64_t& value);
	Flot& operator>>(uint64_t& value);
	Flot& operator>>(float& value);
	Flot& operator>>(double& value);
	Flot& operator>>(std::string& value);
};

}

#endif