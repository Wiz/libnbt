#ifndef __GZFILE__
#define __GZFILE__

#include <flot.hpp>
#include <zlib.h>

namespace NBT
{

class GZFile : public Flot
{
protected:
	gzFile file;
public:
	GZFile(const std::string& filename, const char* mode);
	virtual ~GZFile();

	void open(const std::string& filename, const char* mode);
	int read(const uint8_t* buf, size_t len);
	void write(const uint8_t* buf, size_t len);
	void close();
};

}

#endif