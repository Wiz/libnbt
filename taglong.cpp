#include <taglong.hpp>

namespace NBT
{

TagLong::TagLong()
{
}

TagLong::~TagLong()
{
}

void TagLong::read(Flot& f)
{
	f >> _value;
}

void TagLong::write(Flot& f) const
{
    f << _value;
}

}