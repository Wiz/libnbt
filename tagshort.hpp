#ifndef __NBT_TAGSHORT__
#define __NBT_TAGSHORT__

#include <tag.hpp>

namespace NBT
{

class TagShort : public Tag
{
protected:
    int16_t _value;

protected:
    void write(Flot& o);

public:
    TagShort();
    virtual ~TagShort();
	TAG_TYPE type() const { return TAG_SHORT; }

    int16_t value() { return _value; }
    void setValue(int16_t value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif