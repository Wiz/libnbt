#include <tagint.hpp>

namespace NBT
{

TagInt::TagInt()
{
}

TagInt::~TagInt()
{
}

void TagInt::read(Flot& f)
{
	f >> _value;
}

void TagInt::write(Flot& f) const
{
    f << _value;
}

}