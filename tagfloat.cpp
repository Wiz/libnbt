#include <tagfloat.hpp>

namespace NBT
{

TagFloat::TagFloat()
{
}

TagFloat::~TagFloat()
{
}

void TagFloat::read(Flot& f)
{
	f >> _value;
}

void TagFloat::write(Flot& f) const
{
    f << _value;
}

}