#ifndef __NBT_TAGSTRING__
#define __NBT_TAGSTRING__

#include <tag.hpp>

namespace NBT
{

class TagString : public Tag
{
protected:
    std::string _value;

public:
    TagString();
    virtual ~TagString();
	TAG_TYPE type() const { return TAG_STRING; }

    const std::string& value() { return _value; }
    void setValue(const std::string& value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif