#include <gzfile.hpp>

namespace NBT
{

GZFile::GZFile(const std::string& filename, const char* mode)
{
	open(filename, mode);
}

GZFile::~GZFile()
{
	close();
}

void GZFile::open(const std::string& filename, const char* mode)
{
	file = gzopen(filename.c_str(), mode);
}

void GZFile::close()
{
	gzclose(file);
}

int GZFile::read(const uint8_t* buf, size_t len)
{
	return gzread(file, (void*)buf, len);
}

void GZFile::write(const uint8_t* buf, size_t len)
{
	gzwrite(file, (void*)buf, len);
}

}