#ifndef __NBT__
#define __NBT__

#include <string>
#include <memory>

#include <flot.hpp>

#include <tag.hpp>
#include <tagend.hpp>
#include <tagbyte.hpp>
#include <tagshort.hpp>
#include <tagint.hpp>
#include <taglong.hpp>
#include <tagfloat.hpp>
#include <tagdouble.hpp>
#include <tagbytearray.hpp>
#include <tagstring.hpp>
#include <taglist.hpp>
#include <tagcompound.hpp>

namespace NBT
{

std::shared_ptr<TagCompound> read(Flot& b);
std::shared_ptr<TagCompound> read(const std::string& filename);
void write(const Tag& nbt, Flot& b);
void write(const std::shared_ptr<Tag>&, const std::string& filename);

}

#endif /*__NBT__*/
