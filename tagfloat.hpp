#ifndef __NBT_TAGFLOAT__
#define __NBT_TAGFLOAT__

#include <tag.hpp>

namespace NBT
{

class TagFloat : public Tag
{
protected:
    float _value;

protected:
    void write(Flot& o);

public:
    TagFloat();
    virtual ~TagFloat();
	TAG_TYPE type() const { return TAG_FLOAT; }

    float value() { return _value; }
    void setValue(float value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif