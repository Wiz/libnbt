#include <tagdouble.hpp>

namespace NBT
{

TagDouble::TagDouble()
{
}

TagDouble::~TagDouble()
{
}

void TagDouble::read(Flot& f)
{
	f >> _value;
}

void TagDouble::write(Flot& f) const
{
    f << _value;
}

}