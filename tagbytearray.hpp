#ifndef __NBT_TAGBYTEARRAY__
#define __NBT_TAGBYTEARRAY__

#include <vector>
#include <tag.hpp>

namespace NBT
{

class TagByteArray : public Tag
{
protected:
    std::vector<uint8_t> _data;

public:
    TagByteArray();
    virtual ~TagByteArray();
	TAG_TYPE type() const { return TAG_BYTE_ARRAY; }

    std::vector<uint8_t>& data() { return _data; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif