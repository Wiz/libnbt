#ifndef __NBT_TAGINT__
#define __NBT_TAGINT__

#include <tag.hpp>

namespace NBT
{

class TagInt : public Tag
{
protected:
    int32_t _value;

protected:
    void write(Flot& o);

public:
    TagInt();
    virtual ~TagInt();
	TAG_TYPE type() const { return TAG_INT; }

    int32_t value() { return _value; }
    void setValue(int32_t value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif