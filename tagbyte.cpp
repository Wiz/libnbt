#include <tagbyte.hpp>

namespace NBT
{

TagByte::TagByte()
{
}

TagByte::~TagByte()
{
}

void TagByte::read(Flot& f)
{
	f >> _value;
}

void TagByte::write(Flot& f) const
{
    f << _value;
}

}