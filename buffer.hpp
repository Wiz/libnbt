#ifndef __BUFFER__
#define __BUFFER__

#include <cstddef>
#include <stdint.h>
#include <string>
#include <vector>

class Buffer
{
protected:
	std::vector<uint8_t> buffer;
	size_t readPos;
public:
	Buffer();
	virtual ~Buffer();

	void read(const uint8_t* data, std::size_t size);
	void write(const uint8_t* data, std::size_t size);
	void clear();
	const uint8_t* data();
	std::size_t size();

	Buffer& operator<<(int8_t value);
	Buffer& operator<<(uint8_t value);
	Buffer& operator<<(int16_t value);
	Buffer& operator<<(uint16_t value);
	Buffer& operator<<(int32_t value);
	Buffer& operator<<(uint32_t value);
	Buffer& operator<<(float value);
	Buffer& operator<<(const std::string& value);

	Buffer& operator>>(int8_t value);
	Buffer& operator>>(uint8_t value);
	Buffer& operator>>(int16_t value);
	Buffer& operator>>(uint16_t value);
	Buffer& operator>>(int32_t value);
	Buffer& operator>>(uint32_t value);
	Buffer& operator>>(float value);
	Buffer& operator>>(const std::string& value);

};

#endif /*__BUFFER__*/