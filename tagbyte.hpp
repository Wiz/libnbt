#ifndef __NBT_TAGBYTE__
#define __NBT_TAGBYTE__

#include <tag.hpp>

namespace NBT
{

class TagByte : public Tag
{
protected:
    int8_t _value;

protected:
    void write(Flot& o);

public:
    TagByte();
    virtual ~TagByte();
	TAG_TYPE type() const { return TAG_BYTE; }

    int8_t value() { return _value; }
    void setValue(int8_t value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif