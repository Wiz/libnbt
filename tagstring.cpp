#include <tagstring.hpp>

namespace NBT
{

TagString::TagString()
{
}

TagString::~TagString()
{
}

void TagString::read(Flot& f)
{
	f >> _value;
}

void TagString::write(Flot& f) const
{
	f << _value;
}

}