#include <tagshort.hpp>

namespace NBT
{

TagShort::TagShort()
{
}

TagShort::~TagShort()
{
}

void TagShort::read(Flot& f)
{
	f >> _value;
}

void TagShort::write(Flot& f) const
{
    f << _value;
}

}