#include <tag.hpp>
#include <tagend.hpp>
#include <tagbyte.hpp>
#include <tagshort.hpp>
#include <tagint.hpp>
#include <taglong.hpp>
#include <tagfloat.hpp>
#include <tagdouble.hpp>
#include <tagbytearray.hpp>
#include <tagstring.hpp>
#include <taglist.hpp>
#include <tagcompound.hpp>

namespace NBT
{

Tag::Tag()
{
}

Tag::~Tag()
{
}

std::shared_ptr<Tag> createTag(uint8_t type)
{
	switch(type)
	{
	case TAG_END:
		return std::make_shared<TagEnd>();
    case TAG_BYTE:
		return std::make_shared<TagByte>();
    case TAG_SHORT:
		return std::make_shared<TagShort>();
    case TAG_INT:
		return std::make_shared<TagInt>();
    case TAG_LONG:
		return std::make_shared<TagLong>();
    case TAG_FLOAT:
		return std::make_shared<TagFloat>();
    case TAG_DOUBLE:
		return std::make_shared<TagDouble>();
    case TAG_BYTE_ARRAY:
		return std::make_shared<TagByteArray>();
    case TAG_STRING:
		return std::make_shared<TagString>();
    case TAG_LIST:
		return std::make_shared<TagList>();
    case TAG_COMPOUND:
		return std::make_shared<TagCompound>();
    case TAG_INT_ARRAY:
		throw "Not implemented";
	default:
		throw "Invalid tag type";
	}
}

std::shared_ptr<Tag> readTag(Flot& f)
{
	uint8_t type;
	f >> type;
	std::shared_ptr<Tag> tag = createTag(type);
	if(type != TAG_END)
	{
		std::string name;
		f >> name;
		tag->setName(name);
		f >> *tag;
	}

	return tag;
}

void writeTag(const std::shared_ptr<Tag>& tag, Flot& f)
{
	uint8_t type = tag->type();
	f << type;
	if(type != TAG_END)
	{
		f << tag->name();
		f << *tag;
	}
}

}