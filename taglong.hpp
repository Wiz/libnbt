#ifndef __NBT_TAGLONG__
#define __NBT_TAGLONG__

#include <tag.hpp>

namespace NBT
{

class TagLong : public Tag
{
protected:
    int64_t _value;

protected:
    void write(Flot& o);

public:
    TagLong();
    virtual ~TagLong();
	TAG_TYPE type() const { return TAG_LONG; }

    int64_t value() { return _value; }
    void setValue(int64_t value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif