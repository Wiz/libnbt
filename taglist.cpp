#include <taglist.hpp>

namespace NBT
{

TagList::TagList()
{
}

TagList::~TagList()
{
}

void TagList::setListType(TAG_TYPE listType)
{
	_listType = listType;
}

TAG_TYPE TagList::listType()
{
	return _listType;
}

void TagList::read(Flot& f)
{
	uint8_t type;
	f >> type;
	_listType = (TAG_TYPE)type;

	uint32_t size;
	f >> size;

	_data.clear();
	_data.resize(size);

    for(unsigned int i = 0; i < size; i++)
	{
		std::shared_ptr<Tag> tag = createTag(_listType);
		f >> *tag;
		_data[i] = tag;
	}
}

void TagList::write(Flot& f) const
{
	uint8_t type = _listType;
	f << type;

	uint32_t size = _data.size();
	f << size;

    for(unsigned int i = 0; i < size; i++)
	{
		std::shared_ptr<Tag> tag = _data[i];
		if (tag->type() != _listType)
			throw "Wrong type for element";
		f << *tag;
	}
}

}