#ifndef __NBT_TAG__
#define __NBT_TAG__

#include <string>
#include <memory>

#include <flot.hpp>

namespace NBT
{

enum TAG_TYPE {
    TAG_END = 0,
    TAG_BYTE = 1,
    TAG_SHORT = 2,
    TAG_INT = 3,
    TAG_LONG = 4,
    TAG_FLOAT = 5,
    TAG_DOUBLE = 6,
    TAG_BYTE_ARRAY = 7,
    TAG_STRING = 8,
    TAG_LIST = 9,
    TAG_COMPOUND = 10,
    TAG_INT_ARRAY = 11
};

class Tag
{
protected:
    std::string _name;

public:
    Tag();
    virtual ~Tag();

    void setName(const std::string& name) { _name = name; }
    const std::string& name() const { return _name; }

	virtual void read(Flot& f) = 0;
    virtual void write(Flot& f) const = 0;

    virtual TAG_TYPE type() const = 0;
};

inline Flot& operator<<(Flot& f, const Tag &tag) {
    tag.write(f);
    return f;
}

inline Flot& operator>>(Flot& f, Tag &tag) {
    tag.read(f);
    return f;
}

std::shared_ptr<Tag> createTag(uint8_t type);
std::shared_ptr<Tag> readTag(Flot& f);
void writeTag(const std::shared_ptr<Tag>& tag, Flot& f);

}

#endif