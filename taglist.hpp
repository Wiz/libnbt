#ifndef __NBT_TAGLIST__
#define __NBT_TAGLIST__

#include <vector>
#include <tag.hpp>

namespace NBT
{

class TagList : public Tag
{
protected:
    std::vector<std::shared_ptr<Tag>> _data;
	TAG_TYPE _listType;

public:
    TagList();
    virtual ~TagList();
	TAG_TYPE type() const { return TAG_LIST; }

	void setListType(TAG_TYPE listType);
	TAG_TYPE listType();

    std::vector<std::shared_ptr<Tag>>& data() { return _data; }
	
	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif