#ifndef __NBT_TAGCOMPOUND__
#define __NBT_TAGCOMPOUND__

#include <map>
#include <memory>

#include <tag.hpp>

namespace NBT
{

class TagCompound : public Tag
{
protected:
	std::map<std::string, std::shared_ptr<Tag>> children;
public:
    TagCompound();
    virtual ~TagCompound();
    TAG_TYPE type() const { return TAG_COMPOUND; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;

	void set(const std::string& name, const std::shared_ptr<Tag>& value);
	void set(const std::shared_ptr<Tag>& value);
	const std::shared_ptr<Tag> get(const std::string& name);
};

}

#endif