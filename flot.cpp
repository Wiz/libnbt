#include <flot.hpp>

namespace NBT
{

uint16_t htobe16(uint16_t v) {
    char data[2];
    data[1] = v & 0xFF;
	data[0] = (v >> 8) & 0xFF;
    return (*((uint16_t*)data));
}

uint16_t be16toh(uint16_t v) {
	char* data = (char*)&v;
	return (data[0] << 8) + data[1];
}

uint32_t htobe32(uint32_t v) {
    char data[4];
    data[3] = v & 0xFF;
	data[2] = (v >> 8) & 0xFF;
	data[1] = (v >> 16) & 0xFF;
	data[0] = (v >> 24) & 0xFF;
    return (*((uint32_t*)data));
}

uint32_t be32toh(uint32_t v) {
	char* data = (char*)&v;
	return (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
}

float htobef(float vf) {
	uint32_t v = *((uint32_t*)(&vf));
    char data[4];
    data[3] = v & 0xFF;
	data[2] = (v >> 8) & 0xFF;
	data[1] = (v >> 16) & 0xFF;
	data[0] = (v >> 24) & 0xFF;
    return (*((float*)data));
}

float beftoh(float v) {
	char* data = (char*)&v;
	uint32_t ret = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
	return *((float*)(&ret));
}

uint64_t htobe64(uint64_t v) {
    char data[8];
    data[0] = v & 0xFF;
	data[1] = (v >> 8) & 0xFF;
	data[2] = (v >> 16) & 0xFF;
	data[3] = (v >> 24) & 0xFF;
    data[4] = (v >> 32) & 0xFF;
	data[5] = (v >> 40) & 0xFF;
	data[6] = (v >> 48) & 0xFF;
	data[7] = (v >> 56) & 0xFF;
    return (*((uint64_t*)data));
}

uint64_t be64toh(uint64_t v) {
	char* data = (char*)&v;
	return (uint64_t(data[0]) << 56) + (uint64_t(data[1]) << 48) + (uint64_t(data[2]) << 32) + (data[3] << 24) + (data[4] << 16) + (data[5] << 8) + data[6];
}

Flot::Flot()
{
}

Flot::~Flot()
{
}

Flot& Flot::operator<<(int8_t value)
{
	write((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator<<(uint8_t value)
{
	write((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator<<(int16_t value)
{
	uint16_t value2 = htobe16(value);
	write((const uint8_t*)&value2, sizeof(value2));

	return *this;
}

Flot& Flot::operator<<(uint16_t value)
{
	uint16_t value2 = htobe16(value);
	write((const uint8_t*)&value2, sizeof(value2));

	return *this;
}

Flot& Flot::operator<<(int32_t value)
{
	int32_t value2 = htobe32(value);
	write((const uint8_t*)&value2, sizeof(value));

	return *this;
}

Flot& Flot::operator<<(uint32_t value)
{
	uint32_t value2 = htobe32(value);
	write((const uint8_t*)&value2, sizeof(value2));

	return *this;
}

Flot& Flot::operator<<(int64_t value)
{
	int64_t value2 = htobe64(value);
	write((const uint8_t*)&value2, sizeof(value));

	return *this;
}

Flot& Flot::operator<<(uint64_t value)
{
	uint64_t value2 = htobe64(value);
	write((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator<<(float value)
{
	float value2 = htobef(value);
	write((const uint8_t*)&value2, sizeof(value2));

	return *this;
}

Flot& Flot::operator<<(double value)
{
	write((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator<<(const std::string& value)
{
	uint16_t len = value.length();
	*this << len;
	write((const uint8_t*)value.c_str(), len);

	return *this;
}

Flot& Flot::operator>>(int8_t& value)
{
	read((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator>>(uint8_t& value)
{
	read((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator>>(int16_t& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = be16toh(value);

	return *this;
}

Flot& Flot::operator>>(uint16_t& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = be16toh(value);

	return *this;
}

Flot& Flot::operator>>(int32_t& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = be32toh(value);

	return *this;
}

Flot& Flot::operator>>(uint32_t& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = be32toh(value);

	return *this;
}

Flot& Flot::operator>>(int64_t& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = be64toh(value);

	return *this;
}

Flot& Flot::operator>>(uint64_t& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = be64toh(value);

	return *this;
}

Flot& Flot::operator>>(float& value)
{
	read((const uint8_t*)&value, sizeof(value));
	value = beftoh(value);

	return *this;
}

Flot& Flot::operator>>(double& value)
{
	read((const uint8_t*)&value, sizeof(value));

	return *this;
}

Flot& Flot::operator>>(std::string& value)
{
	uint16_t len;
	*this >> len;

	char* str = new char[len];
	read((const uint8_t*)str, len);

	value.assign(str, len);

	delete[] str;

	return *this;
}

}