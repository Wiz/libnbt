#include <Buffer.hpp>

int16_t htobe(int16_t v) {
    char data[2];
    data[0] = (v >> 8) & 0xFF;
    data[1] = v & 0xFF;
    return (*((int16_t*)data));
}

int32_t htobe(int32_t v) {
    char data[4];
    data[0] = (v >> 24) & 0xFF;
    data[1] = (v >> 16) & 0xFF;
    data[2] = (v >> 8) & 0xFF;
    data[3] = v & 0xFF;
    return (*((int32_t*)data));
}

Buffer::Buffer()
	: readPos(0)
{
}

Buffer::~Buffer()
{
}

void Buffer::read(const uint8_t* data, std::size_t size)
{
	size_t newReadPos = readPos + size;
	if (newReadPos >= buffer.size())
		throw "Can't read that much.";
	data = &buffer[readPos];
	readPos = newReadPos;
}

void Buffer::write(const uint8_t* data, std::size_t size)
{
	if(size > 0) {
		std::size_t pos = buffer.size();
		buffer.resize(pos + size);
		std::memcpy(&buffer[pos], data, size);
	}
}

void Buffer::clear()
{
	buffer.clear();
}

const uint8_t* Buffer::data()
{
	return &buffer[0];
}

std::size_t Buffer::size()
{
	return buffer.size();
}

Buffer& Buffer::operator<<(int8_t value)
{
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(uint8_t value)
{
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(int16_t value)
{
	value = htobe(value);
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(uint16_t value)
{
	value = htobe(value);
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(int32_t value)
{
	value = htobe(value);
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(uint32_t value)
{
	value = htobe((int32_t)value);
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(float value)
{
	write((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator<<(const std::string& value)
{
	uint16_t len = value.length();
	write((uint8_t*)&len, sizeof(len));
	write((uint8_t*)value.c_str(), len);

	return *this;
}

Buffer& Buffer::operator>>(int8_t value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(uint8_t value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(int16_t value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(uint16_t value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(int32_t value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(uint32_t value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(float value)
{
	read((uint8_t*)&value, sizeof(value));

	return *this;
}

Buffer& Buffer::operator>>(const std::string& value)
{
	uint16_t len = value.length();
	write((uint8_t*)&len, sizeof(len));
	write((uint8_t*)value.c_str(), len);

	return *this;
}