#include <nbt.hpp>
#include <gzfile.hpp>

namespace NBT
{

std::shared_ptr<TagCompound> read(Flot& f)
{
	return std::dynamic_pointer_cast<TagCompound>(readTag(f));
}

std::shared_ptr<TagCompound> read(const std::string& filename)
{
	return read(GZFile(filename, "rb"));
}

void write(const std::shared_ptr<Tag>& root, Flot& f)
{
	writeTag(root, f);
}

void write(const std::shared_ptr<Tag>& root, const std::string& filename)
{
	write(root, GZFile(filename, "wb"));
}

}