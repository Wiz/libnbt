#ifndef __NBT_TAGDOUBLE__
#define __NBT_TAGDOUBLE__

#include <tag.hpp>

namespace NBT
{

class TagDouble : public Tag
{
protected:
    double _value;

protected:
    void write(Flot& o);

public:
    TagDouble();
    virtual ~TagDouble();
	TAG_TYPE type() const { return TAG_DOUBLE; }

    double value() { return _value; }
    void setValue(double value) { _value = value; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif