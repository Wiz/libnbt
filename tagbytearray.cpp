#include <tagbytearray.hpp>

namespace NBT
{

TagByteArray::TagByteArray()
{
}

TagByteArray::~TagByteArray()
{
}

void TagByteArray::read(Flot& f)
{
	uint32_t size;
	f >> size;

	_data.resize(size);
	f.read(&_data[0], size);
}

void TagByteArray::write(Flot& f) const
{
	uint32_t size = _data.size();
	f << size;

	f.write(&_data[0], size);
}

}