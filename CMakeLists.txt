cmake_minimum_required(VERSION 2.8)

find_package(ZLIB REQUIRED)
include_directories(${ZLIB_INCLUDE_DIRS})

include_directories(.)
add_library(nbt
	nbt.cpp nbt.hpp
	buffer.cpp buffer.hpp
	flot.cpp flot.hpp
	gzfile.cpp gzfile.hpp
	tag.cpp tag.hpp
	tagend.cpp tagend.hpp
	tagbyte.cpp tagbyte.hpp
	tagshort.cpp tagshort.hpp
	tagint.cpp tagint.hpp
	taglong.cpp taglong.hpp
	tagfloat.cpp tagfloat.hpp
	tagdouble.cpp tagdouble.hpp
	tagbytearray.cpp tagbytearray.hpp
	tagstring.cpp tagstring.hpp
	taglist.cpp taglist.hpp
	tagcompound.cpp tagcompound.hpp
)
target_link_libraries(nbt ${ZLIB_LIBRARIES})
