#ifndef __NBT_TAGEND__
#define __NBT_TAGEND__

#include <tag.hpp>

namespace NBT
{

class TagEnd : public Tag
{
public:
    TagEnd();
    virtual ~TagEnd();
    TAG_TYPE type() const { return TAG_END; }

	virtual void read(Flot& f) override;
    virtual void write(Flot& f) const override;
};

}

#endif